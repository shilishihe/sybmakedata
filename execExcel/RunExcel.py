from datetime import date
import pandas as pd
import datetime


class RunExcel:

    def __init__(self) -> None:
        self.excelFileName = '非贫困户农业人口台帐原版.xlsx'
        self.tableName = 'Sheet1'
        self.excelFilterFileName = '非贫困户农业人口台帐筛选后版本.xlsx'
        self.excelTabl = pd.read_excel(self.excelFileName, sheet_name=self.tableName)
        self.excelTablID = self.excelTabl['身份证号码（或年龄）']
        super().__init__()

    def calculate_age(self, born):
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    def wirte_insertAgeColumn(self, datas):
        self.excelTabl['年龄'] = datas
        self.excelTabl.to_excel(self.excelFileName, sheet_name=self.tableName, index=False, header=True)

    def wirte_dropRangeAgeRows(self):
        excelTablAge = self.excelTabl['年龄']
        for index in range(len(excelTablAge)):
            age = int(excelTablAge[index])
            if age < 16:
                self.excelTabl = self.excelTabl.drop([index], axis=0)
            elif age > 59:
                self.excelTabl = self.excelTabl.drop([index], axis=0)
        self.excelTabl.to_excel(self.excelFilterFileName, sheet_name=self.tableName, index=True, header=True)

    def mainRun(self):
        ages = []
        for index in range(len(self.excelTablID)):
            oriID = str(self.excelTablID[index])
            if len(oriID) < 18:
                ages.append('0')
            else:
                born = datetime.datetime.strptime(oriID[6:14], '%Y%m%d')
                ages.append(self.calculate_age(born))
        self.wirte_insertAgeColumn(ages)
        self.wirte_dropRangeAgeRows()


runExcel = RunExcel()
runExcel.mainRun()
