from add_order_test import AddOrderTest
from order_signing import OrderSigningTest
from performance_confir_test import PerformanceConfirTest
from factory_apply import Factory_Apply
from audit_order_shop import AuditOrderTest
from audit_order__by_finance import AuditOrder
import time


class StartWorkflow():
    def startwork(self):
        while True:
            try:
                print("1.录入订单，2.签约，3.业绩确认，4.申请闪佣，5.城总以及会计审核完成，6.业务风控以及金融风控审核完成,(按Q/q退出)")
                i = input("请输入需要数据的结束节点编号：\n")
                if i == '1':
                    # 多多新房录订单：产生订单ID
                    ao = AddOrderTest()
                    self.orderId = ao.add_order()
                    print("订单录入成功")
                elif i == '2':
                    # 多多新房录订单：产生订单ID
                    ao = AddOrderTest()
                    self.orderId = ao.add_order()
                    print("订单录入成功")
                    # 订单签约 需要订单ID
                    os = OrderSigningTest()
                    os.signing_confirm(self.orderId)
                    print("订单签约成功")
                elif i == '3':
                    # 多多新房录订单：产生订单ID
                    ao = AddOrderTest()
                    self.orderId = ao.add_order()
                    print("订单录入成功")
                    # 订单签约 需要订单ID
                    os = OrderSigningTest()
                    os.signing_confirm(self.orderId)
                    print("订单签约成功")
                    # 订单业绩确认 订单ID和申请ID
                    pc = PerformanceConfirTest()
                    pc.test_performance_confir(pc.performance_confir_apply(self.orderId))
                    time.sleep(1)
                    print("订单业绩确认成功")
                elif i == '4':
                    # 多多新房录订单：产生订单ID
                    ao = AddOrderTest()
                    self.orderId = ao.add_order()
                    print("订单录入成功")
                    # 订单签约 需要订单ID
                    os = OrderSigningTest()
                    os.signing_confirm(self.orderId)
                    print("订单签约成功")
                    # 订单业绩确认 订单ID和申请ID
                    pc = PerformanceConfirTest()
                    pc.test_performance_confir(pc.performance_confir_apply(self.orderId))
                    time.sleep(1)
                    print("订单业绩确认成功")
                    # 发起闪佣  订单ID
                    fa = Factory_Apply()
                    fa.factory_apply(self.orderId)
                    time.sleep(1)
                    print("订单申请闪佣成功")
                elif i == '5':
                    # 多多新房录订单：产生订单ID
                    ao = AddOrderTest()
                    self.orderId = ao.add_order()
                    print("订单录入成功")
                    # 订单签约 需要订单ID
                    os = OrderSigningTest()
                    os.signing_confirm(self.orderId)
                    print("订单签约成功")
                    # 订单业绩确认 订单ID和申请ID
                    pc = PerformanceConfirTest()
                    pc.test_performance_confir(pc.performance_confir_apply(self.orderId))
                    time.sleep(1)
                    print("订单业绩确认成功")
                    # 发起闪佣  订单ID
                    fa = Factory_Apply()
                    fa.factory_apply(self.orderId)
                    time.sleep(1)
                    print("订单闪佣申请成功")
                    # 城总和城市会计审核 申请ID
                    aot = AuditOrderTest()
                    aot.audit_order(self.orderId)
                    time.sleep(5)
                    print("订单商户系统审核完成")
                elif i == '6':
                    # 多多新房录订单：产生订单ID
                    ao = AddOrderTest()
                    self.orderId = ao.add_order()
                    print("订单录入成功")
                    # 订单签约 需要订单ID
                    os = OrderSigningTest()
                    os.signing_confirm(self.orderId)
                    print("订单签约成功")
                    # 订单业绩确认 订单ID和申请ID
                    pc = PerformanceConfirTest()
                    pc.test_performance_confir(pc.performance_confir_apply(self.orderId))
                    time.sleep(1)
                    print("订单业绩确认成功")
                    # 发起闪佣  订单ID
                    fa = Factory_Apply()
                    fa.factory_apply(self.orderId)
                    time.sleep(1)
                    print("订单闪佣申请成功")
                    # 城总和城市会计审核 申请ID
                    aot = AuditOrderTest()
                    aot.audit_order(self.orderId)
                    print("订单商户系统审核完成")
                    # 业务风控和金融风控审核 佣金ID
                    aof = AuditOrder()
                    time.sleep(12)
                    aof.audit_order_by_finance(self.orderId)
                    print("订单已准入完成")

                elif i == 'q' or i == 'Q':
                    break
                    # str.lower():返回将字符串中所有大写字符转换为小写后生成的字符串
                else:
                    i = int(input("输入数字编号与说明信息不匹配，按回车重新输入！"))
            except Exception as e:
                # print(e)
                print("输入信息有误，请输入对应的数字编号！\n")

                # 房佣宝发起请佣  订单ID

                # 数据专员和财务审核放款申请  请佣单号

                # 出纳放款经纪公司  请佣单号

                # 收开发商款

        # 还资金渠道款申请单生成

        # 还款单审核

        # 还款


a = StartWorkflow()
a.startwork()
