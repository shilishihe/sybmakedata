import requests
from readConfig import ReadConfig
from jgj_login import Login
import time
from sql_execution import SqlExecution

# 订单准入审核接口
class Ordertest():
    def __init__(self):
        # 定义所有需要的URL链接
        self.base_url_sms = "http://10.0.4.66:61020/api/finance/sys/sms"

        self.base_url_business = "http://10.0.4.66:61020/api/finance/fund/account/transfer/run"

        self.case_name = ""
        #获取金管家登录态
        self.login = Login ()
    def audit_order_by_finance(self):
        '''订单准入业务风控&金融风控审核'''

        # 定义请求参数
        self.body_sms = {"mobile":13058019302,"type":2,"validTime":5000}
        self.body_data = {"applyId":10135,"userId":6,"mobile":13058019302,"verifyCode":"1234"}
        '''获取金管家登录态'''
        self.login.get_session()
        #业务风控审核
        self.r = requests.post (self.base_url_sms, json=self.body_sms)
        self.result = self.r.json ()
        print (self.result)
        # time.sleep(2)
        self.r2 = requests.post(self.base_url_business, json=self.body_data)
        self.result1 = self.r2.json ()
        print (self.result1)
        self.r = requests.post (self.base_url_sms, json=self.body_sms)
        self.result = self.r.json ()
        print (self.result)
        self.r3 = requests.post (self.base_url_business, json=self.body_data)
        self.result3 = self.r3.json ()
        print (self.result3)

a=Ordertest()
a.audit_order_by_finance()