# -*- coding:utf-8 -*-
import requests
from readConfig import ReadConfig
from ddxf_app_login import DdxfLogin

# 订单转签约接口
class OrderSigningTest ():
    global login
    rc = ReadConfig()
    login = DdxfLogin()
    def __init__(self):
        # 定义所有需要的URL链接
        self.ip = self.rc.get_url ("nh_IP")
        self.port = self.rc.get_url ('nh_app_Port')
        self.base_url = "http://"+self.ip +":"+self.port+"/ddxf/order"
        self.base_url_app_login = "http://"+self.ip +":"+self.port+"/ddxf/user/login"
        self.case_name = " "

    def signing_confirm(self,orderId):
        '''签约操作流程'''
        self.head = {"Content-Type": "application/json",
                     "version": "4.0.4",
                     "apiVersion": "4.0.4",
                     "platform": "iOS",
                     "deviceId": "A3D2498C-E831-4CCF-BC08-964CE5F0D9EF",
                     "platformVersion": "11.4",
                     "token": login.login_app(),
                     "userId": "408899",
                     "ocUserId": "408899"
                     }
        self.base_url_signing = "http://"+self.ip +":"+self.port+"/ddxf/order/deal/status/%s" % orderId
        self.body_data_signing = {"actionType": 2, "buildingNo": "A栋", "contractAmount": 20000000,
                                  "contractArea": "66.00", "eventTime": 1546072646199, "flatId": 125512,
                                  "houseId": 54608, "houseNo": "111", "houseResourceId": 33698,
                                  "orderAttachmentInput": {
                                      "contractImgUrls": ["https://fsupload.fangdd.net/image/U-ie650sSoCPrBNhhZs3RPvXIJY.jpg"],
                                      "custIdCardImgUrls": [], "otherUrls": [],
                                      "subcribeImgUrls": ["https://fsupload.fangdd.net/image/ipIorj4aQ-WnzaNlP7H6dOQNovM.jpg"],
                                      "subcribeReceiptImgUrls": [], "type": 20 },
                                  "orderNote": "小木鸟的签约操作", "packageId": 501432,"orderType":2,
                                  "predictTime": 1540224000000, "salesAmount": 10000000, "unitNo": "1227001"
                                  }
        self.r = requests.post (self.base_url_signing,json=self.body_data_signing,headers=self.head)
        result = self.r.json()
        print (result)


# a= OrderSigningTest()
# a.signing_confirm(1492139)

