# -*- coding:utf-8 -*-
import requests
from sql_execution import SqlExecution
from ddxf_app_login import DdxfLogin



# 订单准入审核接口
class AuditOrderTest ():

    def __init__(self):
        # 定义所有需要的URL链接
        self.base_url = "http://10.0.4.104:12008/ddxf/work/audit"
        self.case_name = " "
        #登录多多新房APP
        self.a = DdxfLogin()
        self.token = self.a.login_app()



    def audit_order(self,orderId):
        '''订单准入审核'''
        self.head = {"Content-Type": "application/json",
                     "version": "4.0.4",
                     "apiVersion": "4.0.4",
                     "platform": "iOS",
                     "deviceId": "A3D2498C-E831-4CCF-BC08-964CE5F0D9EF",
                     "platformVersion": "11.4",
                     "token": self.token,
                     "userId": "408899",
                     "ocUserId": "408899"
                     }

        # 获取佣金申请ID
        self.a = SqlExecution ()
        self.businessId = self.a.get_value ("SELECT t.`apply_id` FROM `fdd_xfbp_factoring`.`commission_factoring` t WHERE t.`order_id`=%s" % orderId)
        print(self.businessId)
        # 定义请求参数
        self.body_data = {"remark": "小木鸟审核通过！", "auditStatus": 1, "processType": 17, "businessId": self.businessId,"pageNo": 0, "pageSize": 10}

        i=0
        while i<2:
            self.r = requests.post(self.base_url,json=self.body_data,headers=self.head)
            self.result = self.r.json()
            print(self.result)
            i+=1


# a = AuditOrderTest()
# a.audit_order(1492196)