# -*- coding:utf-8 -*-
import requests
from readConfig import ReadConfig
from jgj_login import Login
import time
from sql_execution import SqlExecution





# 订单准入审核接口
class AuditOrder():
    rc = ReadConfig()
    def __init__(self):
        # 定义所有需要的URL链接
        self.ip = self.rc.get_url ("jgj_IP")
        self.port = self.rc.get_url ("jgj_Port")
        self.base_url_business = "http://" +self.ip+ ":"+ self.port + "/api/order/apply/business/acceptance"
        self.base_url_finance = "http://" + self.ip + ":" + self.port + "/api/order/apply/finance/acceptance"
        self.case_name = ""
        #获取金管家登录态
        self.login = Login ()
    def audit_order_by_finance(self,orderId):
        '''订单准入业务风控&金融风控审核'''
        # 根据订单ID获取对应的佣金ID
        self.a = SqlExecution ()
        self.programId = self.a.get_value (
            "SELECT t.`commission_factoring_id` FROM `fdd_xfbp_factoring`.`commission_factoring` t WHERE t.`status`= 1 AND t.`order_id`=%s" % orderId)
        print (self.programId)
        # 定义请求参数
        self.body_data = {"userId": 6, "operatorName": "小木鸟", "operationType": "0", "remark": "小木鸟审核通过", "programId": self.programId, "channelId": 4}
        '''获取金管家登录态'''
        self.login.get_session()
        #业务风控审核
        self.r = requests.patch (self.base_url_business, data=self.body_data)
        self.result = self.r.json ()
        print (self.result)
        time.sleep(1)
        #金融风控审核
        self.r = requests.patch (self.base_url_finance, data=self.body_data)
        self.result = self.r.json ()
        print (self.result)

# a = AuditOrder()
# a.audit_order_by_finance(1492210)

