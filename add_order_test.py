# -*- coding:utf-8 -*-
import requests
from readConfig import ReadConfig


# 录订单接口
class AddOrderTest ():
    rc = ReadConfig()
    def __init__(self):
        self.ip = self.rc.get_url("nh_IP")
        self.port = self.rc.get_url('nh_app_Port')
        self.shop_port = self.rc.get_url('nh_shop_Port')
        # 定义所有需要的URL链接
        self.base_url = "http://"+self.ip +":"+self.port+"/ddxf/order"
        self.base_url_app_login = "http://"+self.ip +":"+self.port+"/ddxf/user/login"
        self.base_url_shop_login = "http://"+self.ip +":"+self.shop_port+"/account/login.json"
        self.base_url_shop_login_code_apply = "http://"+self.ip +":"+self.shop_port+"/account/gen_identify_code_image.json"
        self.base_url_shop_login_code = "http://"+self.ip +":"+self.shop_port+"/account/query_identity_code.json"


        self.case_name = " "
        self.iphone = "13333336666"
        # 定义请求参数（手动闪佣结算规则：501432，自动闪佣结算规则：501689）
        self.body_data = {"projectId": 14673, "predictTime": 1540262437000, "custMobile": self.iphone, "houseId": 54608,
                          "dealStatus": 2, "orderTime": 1540262437000, "pageSize": 10, "pageNo": 0, "packageId": 501689,
                          "contractArea": "66", "custName": "李四", "contractAmount": 20000000, "salesAmount": 10000000,
                          "orderAttachmentInput": {"otherUrls": [], "custIdCardImgUrls": [],
                                                   "subcribeReceiptImgUrls": [],
                                                   "subcribeImgUrls": [
                                                       "https://fsupload.fangdd.net/image/pIorj4aQ-WnzaNlP7H6dOQNovM.jpg"]
                                                   }
                          }


    def login_app(self):
        '''登录多多新房APP'''
        self.body_data2 = {"password": "Fdd123@@", "username": "新体验项目助理"}
        self.login = requests.post (self.base_url_app_login, json=self.body_data2)
        # 获取后台返回的token
        self.a = self.login.json ()['data']
        self.token = self.a['token']
        return self.token

    def add_order(self):
        '''录入订单操作'''
        self.head = {"Content-Type": "application/json",
                     "version": "4.0.4",
                     "apiVersion": "4.0.4",
                     "platform": "iOS",
                     "deviceId": "A3D2498C-E831-4CCF-BC08-964CE5F0D9EF",
                     "platformVersion": "11.4",
                     "token": self.login_app (),
                     "userId": "408899",
                     "ocUserId": "408899"
                     }
        r = requests.post (self.base_url, json=self.body_data, headers=self.head)
        result = r.json ()
        # 获取录入成功后的订单ID
        i = result['data']
        orderId = i['orderId']
        print (orderId)
        # 返回订单ID
        return orderId





# if __name__ == '__main__':
#     a = AddOrderTest()
#     a.login_app()
#     a.add_order()
