from readConfig import ReadConfig
from shop_login import ShopLogin


class Factory_Apply ():
    rc = ReadConfig ()

    def __init__(self):
        # 定义所有需要的URL链接
        self.ip = self.rc.get_url ("nh_IP")
        self.apply_url = "http://10.0.4.107:12042/commission/apply/factoring"
        self.case_name = " "
        # 调用登录方法，获取登录session
        self.a = ShopLogin ()
        self.testsession = self.a.login_shop ()

    def factory_apply(self,orderId):
        '''闪佣申请发起'''
        # 定义参数
        self.apply_data = {"applyFactoringDetails": [
            {"orderId": orderId, "commissionType": 2, "applyAmount": 70000, "recvPaymentPredictTime": 1569931791975,
             "discount": 78, "orderPaybackPlanId": 382}
        ], "attachUrls": [], "remark": "小木鸟申请"}
        self.r = self.testsession.post (self.apply_url, json=self.apply_data)
        self.result = self.r.json ()
        print (self.result)


# a=Factory_Apply()
# a.factory_apply(1492139)
